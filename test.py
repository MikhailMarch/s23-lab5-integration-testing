import requests

target_url_with_key = "https://script.google.com/macros/s/AKfycbyCI48MGySXx4FjneAO2ufGjonNLfw95dx-eJ4pSi_oIwy-qcaG0f9U7jse4lh6JM0R/"

my_email = "m.martovitsky@innopolis.university"

# data from requests
budet_per_minute = 27
luxury_per_minute = 72
fixed_per_km = 18
allowed_deviations = 12
discount = 14

max_incorrect = 10001
min_incorrect = -1

max_correct = 10000
min_correct = 0

test_params_list = [
    {
        'type': 'some_type',
        'plan': 'minute',
        'distance': 100,
        'planned_distance': 100,
        'time': 100,
        'planned_time': 100,
        'inno_discount': 'yes'
    },
    {
        'type': 'budget',
        'plan': 'some_plan',
        'distance': 100,
        'planned_distance': 100,
        'time': 100,
        'planned_time': 100,
        'inno_discount': 'yes'
    },
    {
        'type': 'budget',
        'plan': 'minute',
        'distance': min_incorrect,
        'planned_distance': 100,
        'time': 100,
        'planned_time': 100,
        'inno_discount': 'yes'
    },
    {
        'type': 'budget',
        'plan': 'minute',
        'distance': min_correct,
        'planned_distance': 100,
        'time': 100,
        'planned_time': 100,
        'inno_discount': 'yes'
    },
    {
        'type': 'budget',
        'plan': 'minute',
        'distance': max_correct,
        'planned_distance': 100,
        'time': 100,
        'planned_time': 100,
        'inno_discount': 'yes'
    },
{
        'type': 'budget',
        'plan': 'minute',
        'distance': max_incorrect,
        'planned_distance': 100,
        'time': 100,
        'planned_time': 100,
        'inno_discount': 'yes'
    },
    {
        'type': 'budget',
        'plan': 'minute',
        'distance': 100,
        'planned_distance': min_incorrect,
        'time': 100,
        'planned_time': 100,
        'inno_discount': 'yes'
    },
    {
        'type': 'budget',
        'plan': 'minute',
        'distance': 100,
        'planned_distance': min_correct,
        'time': 100,
        'planned_time': 100,
        'inno_discount': 'yes'
    },
    {
        'type': 'budget',
        'plan': 'minute',
        'distance': 100,
        'planned_distance': min_incorrect,
        'time': 100,
        'planned_time': 100,
        'inno_discount': 'yes'
    },
    {
        'type': 'budget',
        'plan': 'minute',
        'distance': 100,
        'planned_distance': max_correct,
        'time': 100,
        'planned_time': 100,
        'inno_discount': 'yes'
    },
    {
        'type': 'budget',
        'plan': 'minute',
        'distance': 100,
        'planned_distance': max_incorrect,
        'time': 100,
        'planned_time': 100,
        'inno_discount': 'yes'
    },
    {
        'type': 'budget',
        'plan': 'minute',
        'distance': 100,
        'planned_distance': 100,
        'time': min_incorrect,
        'planned_time': 100,
        'inno_discount': 'yes'
    },
    {
        'type': 'budget',
        'plan': 'minute',
        'distance': 100,
        'planned_distance': 100,
        'time': min_correct,
        'planned_time': 100,
        'inno_discount': 'yes'
    },
    {
        'type': 'budget',
        'plan': 'minute',
        'distance': 100,
        'planned_distance': 100,
        'time': max_incorrect,
        'planned_time': 100,
        'inno_discount': 'yes'
    },
    {
        'type': 'budget',
        'plan': 'minute',
        'distance': 100,
        'planned_distance': 100,
        'time': max_correct,
        'planned_time': 100,
        'inno_discount': 'yes'
    },
    {
        'type': 'budget',
        'plan': 'minute',
        'distance': 100,
        'planned_distance': 100,
        'time': 100,
        'planned_time': min_incorrect,
        'inno_discount': 'yes'
    },
    {
        'type': 'budget',
        'plan': 'minute',
        'distance': 100,
        'planned_distance': 100,
        'time': 100,
        'planned_time': min_correct,
        'inno_discount': 'yes'
    },
    {
        'type': 'budget',
        'plan': 'minute',
        'distance': 100,
        'planned_distance': 100,
        'time': 100,
        'planned_time': max_incorrect,
        'inno_discount': 'yes'
    },
    {
        'type': 'budget',
        'plan': 'minute',
        'distance': 100,
        'planned_distance': 100,
        'time': 100,
        'planned_time': max_correct,
        'inno_discount': 'yes'
    },
    {
        'type': 'budget',
        'plan': 'minute',
        'distance': 100,
        'planned_distance': 100,
        'time': 100,
        'planned_time': 100,
        'inno_discount': 'kek'
    }
]

def is_valid_type(input_type):
    return input_type == "budget" or input_type == "luxury"

def is_valid_plan(input_plan):
    return input_plan == "fixed_price" or input_plan == "minute"

def is_valid_distance(input_distance):
    return input_distance >= 0

def is_valid_planned_distance(input_distance):
    return input_distance > 0

def is_valid_time(input_time):
    return input_time >= 0

def is_valid_planned_time(input_time):
    return input_time > 0

def is_valid_inno_discount(input_discount):
    return input_discount == "yes" or input_discount == "no"

def get_expected_price(params):
    global budet_per_minute, luxury_per_minute, fixed_per_km, allowed_deviations, discount

    type = params.get('type')
    plan = params.get('plan')
    distance = params.get('distance')
    planned_distance = params.get('planned_distance')
    time = params.get('time')
    planned_time = params.get('planned_time')
    inno_discount = params.get('inno_discount')

    if not is_valid_type(type) or \
        not is_valid_plan(plan) or \
        not is_valid_distance(distance) or \
        not is_valid_planned_distance(planned_distance) or \
        not is_valid_time(time) or \
        not is_valid_planned_time(planned_time) or \
        not is_valid_inno_discount(inno_discount):

        return "Invalid Request"
    price = 0
    if plan == 'fixed_price':
        deviation_distance_in_percent = (planned_distance - distance) / planned_distance * 100
        deviation_time_in_percent = (planned_time - time) / planned_time * 100
        if deviation_distance_in_percent > allowed_deviations or deviation_time_in_percent > allowed_deviations:
            params['plan'] = 'minute'
        else:
            price = planned_distance * fixed_per_km

    if params['plan'] == 'minute':
        if type == 'budget':
            price = time * budet_per_minute
        if type == 'luxury':
            price = time * luxury_per_minute

    if params['inno_discount'] == 'yes':
        return price * (1 - discount / 100)
    elif params['inno_discount'] == 'no':
        return price

def get_real_price(params):
    global target_url_with_key, my_email

    cmd = 'exec?service=calculatePrice&email='

    url = target_url_with_key + cmd + my_email + \
        '&type=' + params['type'] + \
        '&plan=' + params['plan'] + \
        '&distance=' + str(params['distance']) + \
        '&planned_distance=' + str(params['planned_distance']) + \
        '&time=' + str(params['time']) + \
        '&planned_time=' + str(params['planned_time']) + \
        '&inno_discount=' + params['inno_discount']

    result = requests.get(url)

    if result.content == b'Invalid Request': return 'Invalid Request'

    return result.json()['price']


passed_tests = 0
total = 0

def run_test(params, file):
    global passed_tests, total
    expected = get_expected_price(params)
    real = get_real_price(params)

    total = total + 1
    if expected != real:
        file.write(str(total) + ', ' + str(list(params.values())) + ', ' + str(expected) + ', ' + str(real) + ', ' + 'Error\n')
        print(str(total) + '   ' + str(list(params.values())) + '   ' + str(expected) + '   ' + str(real) + '   ' + 'Error')
    else:
        file.write(str(total) + ', ' + str(list(params.values())) + ', ' + str(expected) + ', ' + str(real) + ', ' + 'Success\n')
        print(str(total) + '   ' + str(list(params.values())) + '   ' + str(expected) + '   ' + str(real) + '   ' + 'Success')
        passed_tests = passed_tests + 1


if __name__ == "__main__":
    f = open("result.csv", "w")
    f.write("Test case N, Request URL, Expected result, Obtained result, Verdict\n")
    print("Test case N   Request URL   Expected result   Obtained result   Verdict")
    for req_type in ['budget', 'luxury']:
        for req_plan in ['minute', 'fixed_price']:
            for req_discount in ['yes', 'no']:
                run_test({'type': req_type, 'plan': req_plan, 'distance': 123, 'planned_distance': 123,
                          'time': 123, 'planned_time': 123, 'inno_discount': req_discount}, f)

    for test_param in test_params_list:
        run_test(test_param, f)

    print("Total amount if tests: " + str(total) + ", Passed tests: " + str(passed_tests) + ", Failed tests: " + str(total - passed_tests))

